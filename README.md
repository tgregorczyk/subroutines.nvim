# Subroutines.nvim

## Goal

- My first plugin, playing to get a better grasp on neovim
- Aims at improving the workflow when working with Fortran
- lua require("subroutines").sub() automatically checks if the *subroutine* (/ module / program / function) name is the same as the *end subroutine* name, and corrects it.
- right now, there is no autocmd in the plugin you have to create one yourself like this:

```
vim.api.nvim_create_autocmd("BufWrite", {
    callback = function()
    require("subroutines").sub()
    end,
})
```

- The installation is simple. If you use Lazy then:

```
{
    "https://gitlab.com/tgregorczyk/subroutines.nvim",
    dependencies = { "nvim-treesitter/nvim-treesitter" },
    ft = "fortran"
},
```

## Future developments

- automatically corrects if we get over the 132 characters per line limit
- ?
