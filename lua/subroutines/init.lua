--TODO: last step will be to check for the 132 characters per line rule → this could be another feature of the plugin: if a line is longer than 132chars then we break it
-- check whether the language is fortran / autocmd to execute only on change and in fortran files
-- TODO: make autocmd on events "fortran" and "BufWrite"
local M = {}

local function P(v)
	print(vim.inspect(v))
end

local function get_table_names(query, parser, bufnr)
	local query_nvim = vim.treesitter.query.parse("fortran", query)
	local tree = parser:parse()[1]
	local text_queries = {}
	for _, n in query_nvim:iter_captures(tree:root(), bufnr) do
		local text = vim.treesitter.get_node_text(n, bufnr)
		local start_row, start_col, end_row, end_col = n:range()
		table.insert(text_queries, {
			text = text,
			start_row = start_row,
			start_col = start_col,
			end_row = end_row,
			end_col = end_col,
		})
	end
	return text_queries
end

local function edge_case(type, mytable, table_end, table_beg, k, bufnr)
	-- TODO: change this hardcoded value with mytable[k]["start_col"] but it doesn't work
	local indent = 0
	if type == "subroutine" then
		indent = 4
	end
	table.insert(table_end, k, mytable[k])
	local new_text = table_beg[k]["text"]
	local endy = table_end[k]
	endy["text"] = new_text

	vim.api.nvim_buf_set_lines(
		bufnr,
		endy["start_row"],
		endy["start_row"] + 1,
		false,
		{ string.rep(" ", indent) .. "end " .. type .. " " .. new_text }
	)
	return table_end
end

local function correct(type)
	local bufnr = vim.api.nvim_get_current_buf()
	local parser = require("nvim-treesitter.parsers").get_parser()
	if parser:lang() == "fortran" then
		local Query_sub = ""
		if type == "subroutine" then
			Query_sub = "(" .. type .. "_statement name: (name) @name)"
		else
			Query_sub = "(" .. type .. "_statement (name) @name)"
		end
		local beg_sub = get_table_names(Query_sub, parser, bufnr)
		local query_end = "(end_" .. type .. "_statement (name) @name)"
		local end_sub = get_table_names(query_end, parser, bufnr)
		local size_beg = 0
		local size_end = 0
		-- get the size of the tables
		for _, _ in pairs(beg_sub) do
			size_beg = size_beg + 1
		end
		for _, _ in pairs(end_sub) do
			size_end = size_end + 1
		end
		-- edge case, if we have some: end subroutine #noname
		if size_beg ~= size_end then
			local end_statements = get_table_names("(end_" .. type .. "_statement) @name", parser, bufnr)
			for k, _ in pairs(beg_sub) do
				-- edge case : if endy["start_row"] > beg_sub[k+1]["start_row"]
				if size_beg >= k + 1 then
					if end_sub[k]["start_row"] > beg_sub[k + 1]["start_row"] then
						end_sub = edge_case(type, end_statements, end_sub, beg_sub, k, bufnr)
						size_end = size_end + 1
					end
					-- if the sizes are still not equal that means that the last subroutine is an edge case
				elseif (size_beg == k) and (size_beg ~= size_end) then
					end_sub = edge_case(type, end_statements, end_sub, beg_sub, k, bufnr)
				end
			end
		end
		-- we correct here the end_sub name
		for k, v in pairs(beg_sub) do
			if v["text"] ~= end_sub[k]["text"] then
				local endy = end_sub[k]
				vim.api.nvim_buf_set_text(
					bufnr,
					endy["start_row"],
					endy["start_col"],
					endy["end_row"],
					endy["end_col"],
					{ v["text"] }
				)
			end
		end
	end
end

M.sub = function()
	correct("subroutine")
	correct("module")
	correct("program")
	correct("function")
end

return M
